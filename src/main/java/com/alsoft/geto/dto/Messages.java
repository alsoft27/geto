package com.alsoft.geto.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Messages {

    @JsonProperty("From")
    private Mail from;

    @JsonProperty("To")
    private List<Mail> to;

    @JsonProperty("TemplateID")
    private Integer templateID;

    @JsonProperty("TemplateLanguage")
    private boolean templateLanguage;

    @JsonProperty("Subject")
    private String subject;

    @JsonProperty("Variables")
    private Variables variables;
}

package com.alsoft.geto.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Mail {

    @JsonProperty("Email")
    private String email;

    @JsonProperty("Name")
    private String name;
}

package com.alsoft.geto.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Variables {

    private String user_geto;

    private String confirmation_link;

}

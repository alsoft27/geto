package com.alsoft.geto.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.alsoft.geto.domain.Geto;

public interface GetoRepository extends MongoRepository<Geto, String> {

	@Query("{ 'creator.email' : ?0 }")
	List<Geto> findGetoByName(String name);

}

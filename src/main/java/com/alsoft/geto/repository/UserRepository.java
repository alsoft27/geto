package com.alsoft.geto.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.alsoft.geto.domain.User;

public interface UserRepository extends MongoRepository<User, String> {

}

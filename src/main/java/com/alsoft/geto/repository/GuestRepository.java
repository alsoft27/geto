package com.alsoft.geto.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.alsoft.geto.domain.Guest;

public interface GuestRepository extends MongoRepository<Guest, String> {

}

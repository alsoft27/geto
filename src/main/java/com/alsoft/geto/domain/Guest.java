package com.alsoft.geto.domain;

import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Guest {

    @Id
    private String id;

    private String name;

    private String email;

    private String getoRef;

    private List<Boolean> answers;
}

package com.alsoft.geto.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Option {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private Date date;

    private String option;

}

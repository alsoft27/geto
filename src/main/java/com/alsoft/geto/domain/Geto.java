package com.alsoft.geto.domain;

import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Geto {

	@Id
	private String id;

	private String name;

	private User creator;

	private String description;

	private String place;

	private List<Option> options;

}

package com.alsoft.geto.port;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.alsoft.geto.dto.Mail;
import com.alsoft.geto.dto.Message;
import com.alsoft.geto.dto.Messages;
import com.alsoft.geto.dto.Variables;
import com.fasterxml.jackson.core.JsonProcessingException;

@Component
public class MailPort {

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;

	@Autowired
	private RestTemplate restTemplate;

	public void createEmail(String email, String name, String key) {
		Message msg = new Message();

		Messages messages = new Messages();
		Mail from = new Mail("getoapp@gmail.com", "geto");
		messages.setFrom(from);
		Mail to = new Mail(email, name);
		List<Mail> lto = new ArrayList<Mail>();
		lto.add(to);
		messages.setTo(lto);
		messages.setTemplateID(259863);
		messages.setTemplateLanguage(true);
		messages.setSubject("geto Create");
		Variables variables = new Variables(name + " geto [" + key + "]",
				"https://alsoft27test.herokuapp.com/getos/" + key);
		messages.setVariables(variables);

		List<Messages> lstMsg = new ArrayList<>();
		lstMsg.add(messages);

		msg.setMessages(lstMsg);

		try {
			sendEMail(msg);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void sendEMail(Message message) throws JsonProcessingException {
		try {
			restTemplate = restTemplateBuilder
					.basicAuthorization("165af67a81daafd8a22600bcea380499", "a011819b659be33b261586f4a3e3a78f").build();

			restTemplate.postForEntity("https://api.mailjet.com/v3.1/send", message, Object.class);// call

		} catch (HttpClientErrorException e) {
			e.printStackTrace();
		}

	}

}

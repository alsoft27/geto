package com.alsoft.geto.service;

import java.util.List;

import com.alsoft.geto.domain.Geto;
import com.alsoft.geto.domain.Guest;

public interface GetoService {

	public Geto getGeto(String id);

	public List<Geto> getos();

	public Geto createGeto(Geto geto);

	public Geto saveGeto(Geto geto);

	public Boolean exitsGeto(String idGeto);

	public Guest addGuest(Guest guest) throws Exception;

	public Guest answersGuest(Guest guest) throws Exception;

}

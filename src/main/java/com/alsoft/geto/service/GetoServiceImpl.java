package com.alsoft.geto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alsoft.geto.domain.Geto;
import com.alsoft.geto.domain.Guest;
import com.alsoft.geto.port.MailPort;
import com.alsoft.geto.repository.GetoRepository;
import com.alsoft.geto.repository.GuestRepository;

@Service
public class GetoServiceImpl implements GetoService {

	@Autowired
	private GetoRepository getoRepository;

	@Autowired
	private GuestRepository guestRepository;

	@Autowired
	private MailPort mailPort;

	@Override
	public Geto getGeto(String id) {
		return getoRepository.findOne(id);
	}

	@Override
	public Geto createGeto(Geto geto) {
		Geto newGeto = getoRepository.save(geto);
		sendEmailGetoCreate(newGeto);
		return newGeto;
	}

	@Override
	public Geto saveGeto(Geto geto) {
		return getoRepository.save(geto);
	}

	@Override
	public Boolean exitsGeto(String idGeto) {
		return getoRepository.exists(idGeto);
	}

	@Override
	public Guest addGuest(Guest guest) throws Exception {
		if (this.exitsGeto(guest.getGetoRef())) {
			Guest newGuest = guestRepository.save(guest);
			sendEmailaddGuest(newGuest);
			return newGuest;
		} else {
			throw new Exception();
		}
	}

	@Override
	public Guest answersGuest(Guest guest) throws Exception {
		return guestRepository.save(guest);
	}

	private void sendEmailGetoCreate(Geto geto) {
		mailPort.createEmail(geto.getCreator().getEmail(), geto.getCreator().getUserName(), geto.getId());
	}

	private void sendEmailaddGuest(Guest guest) {
		mailPort.createEmail(guest.getEmail(), guest.getName(), guest.getGetoRef());
	}

	@Override
	public List<Geto> getos() {
		return getoRepository.findAll();
	}
}

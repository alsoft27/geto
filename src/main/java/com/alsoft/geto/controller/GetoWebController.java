package com.alsoft.geto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alsoft.geto.domain.Geto;
import com.alsoft.geto.domain.User;
import com.alsoft.geto.service.GetoServiceImpl;

@Controller
public class GetoWebController {

	@Autowired
	private GetoServiceImpl getoService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/getos")
	public String list(Model model) {
		List<Geto> res = getoService.getos();
		model.addAttribute("getos", res);
		return "getos";
	}

	@RequestMapping(value = "/getos", method = RequestMethod.POST)
	public String getoSave(Geto geto) {
		getoService.createGeto(geto);
		return "getos";
	}

	@RequestMapping(value = "/getos/new")
	public String getoCreate(Model model) {
		Geto geto = new Geto();
		User user = new User();
		geto.setCreator(user);
		model.addAttribute("geto", geto);
		return "getosCreate";
	}

	@RequestMapping(value = "/getos/{id}")
	public String getGeto(@PathVariable String id, Model model) {
		model.addAttribute("geto", getoService.getGeto(id));
		return "geto";
	}

}

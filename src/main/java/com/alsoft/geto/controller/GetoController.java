package com.alsoft.geto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alsoft.geto.domain.Geto;
import com.alsoft.geto.domain.Guest;
import com.alsoft.geto.service.GetoServiceImpl;

@RestController
public class GetoController {

    @Autowired
    private GetoServiceImpl getoService;

    @RequestMapping(value = "/geto", method = RequestMethod.POST)
    public ResponseEntity<Geto> getoCreate(@RequestBody Geto geto) {
        return new ResponseEntity<Geto>(getoService.createGeto(geto), HttpStatus.OK);
    }

    @RequestMapping(value = "/geto/{idGeto}", method = RequestMethod.GET)
    public ResponseEntity<Geto> getoGet(@PathVariable String idGeto) {
        return new ResponseEntity<Geto>(getoService.getGeto(idGeto), HttpStatus.OK);
    }

    @RequestMapping(value = "/geto", method = RequestMethod.GET, params = { "idGeto" })
    public Boolean exitsGeto(@RequestParam String idGeto) {
        return getoService.exitsGeto(idGeto);
    }

    @RequestMapping(value = "/geto", method = RequestMethod.PATCH)
    public ResponseEntity<Guest> getoAddGuest(@RequestBody Guest guest) {
        ResponseEntity<Guest> res = null;
        try {
            res = new ResponseEntity<Guest>(getoService.addGuest(guest), HttpStatus.OK);
        } catch (Exception e) {
            res = new ResponseEntity<Guest>(guest, HttpStatus.BAD_REQUEST);
        }
        return res;
    }

    @RequestMapping(value = "/guestanswers", method = RequestMethod.PATCH)
    public ResponseEntity<Guest> guestAnswerst(@RequestBody Guest guest) {
        ResponseEntity<Guest> res = null;
        try {
            res = new ResponseEntity<Guest>(getoService.answersGuest(guest), HttpStatus.OK);
        } catch (Exception e) {
            res = new ResponseEntity<Guest>(guest, HttpStatus.BAD_REQUEST);
        }
        return res;
    }
}

package com.alsoft.geto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alsoft.geto.domain.Geto;
import com.alsoft.geto.domain.Option;
import com.alsoft.geto.domain.User;
import com.alsoft.geto.repository.GetoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GetoRepositoryTest {

	@Autowired
	private GetoRepository repository;

	@Ignore
	@Test
	public void createGeto() {
		Geto geto = new Geto();
		geto.setName("geto prueba");
		geto.setDescription("description geto prueba");
		User creator = new User();
		creator.setUserName("geto user");
		creator.setEmail("alsoft27@gmail.com");
		geto.setCreator(creator);
		geto.setPlace("place geto prueba");
		List<Option> options = new ArrayList<>();
		Option option = new Option();
		option.setDate(new Date());
		option.setOption("option A");
		options.add(option);
		geto.setOptions(options);
		repository.save(geto);

		List<Geto> getos = repository.findAll();
		System.out.println("----------------->" + getos.get(0));
	}

	@Ignore
	@Test
	public void getGeto() {
		Geto geto = repository.findOne("5a2ef6d4ae20a60e89540342");
		org.junit.Assert.assertEquals(geto.getId(), "5a2ef6d4ae20a60e89540342");
	}

	@Test
	public void findGetobyName() {
		List<Geto> geto = repository.findGetoByName("alsoft27@gmail.com");
		org.junit.Assert.assertEquals(geto.get(0).getId(), "5a9067a5260c08164e477956");
	}

}

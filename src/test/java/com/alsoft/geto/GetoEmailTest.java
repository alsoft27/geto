package com.alsoft.geto;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alsoft.geto.dto.Mail;
import com.alsoft.geto.dto.Message;
import com.alsoft.geto.dto.Messages;
import com.alsoft.geto.dto.Variables;
import com.alsoft.geto.port.MailPort;
import com.fasterxml.jackson.core.JsonProcessingException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GetoEmailTest {

    @Autowired
    MailPort mailPort;

    @Test
    public void sendEmail() throws JsonProcessingException {

        Message msg = new Message();

        Messages messages = new Messages();
        Mail from = new Mail("getoapp@gmail.com", "geto");
        messages.setFrom(from);
        Mail to = new Mail("alsoft27@gmail.com", "Alfredo");
        List<Mail> lto = new ArrayList<Mail>();
        lto.add(to);
        messages.setTo(lto);
        messages.setTemplateID(259863);
        messages.setTemplateLanguage(true);
        messages.setSubject("geto");
        Variables variables = new Variables("Alfredo Barrios", "http://www.google.com");
        messages.setVariables(variables);

        List<Messages> lstMsg = new ArrayList<>();
        lstMsg.add(messages);

        msg.setMessages(lstMsg);

        mailPort.sendEMail(msg);
    }

}
